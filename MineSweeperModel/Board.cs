﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MineSweeperModel
{
    public class Board
    {

        public int Size { get; set; }
        public Cell[,] theGrid;

        public Random rnd = new Random();
        public int rndCheck;

        public Board(int s)
        {
            Size = s;

            theGrid = new Cell[Size, Size];
            for(int i = 0; i< Size; i++)
            {
                for(int j = 0; j<Size; j++)
                {
                    theGrid[i, j] = new Cell(i, j);
                }
            }
        }

        public void assignBombs()
        {
           for (int r = 0; r<Size; r++)
            {
                for (int c = 0; c<Size; c++)
                {
                    rndCheck = rnd.Next(100);

                    if (rndCheck > 80)
                    {
                        theGrid[r, c].hasBomb = true;

                        try { theGrid[r, c - 1].setNextToBomb(); }
                            catch(System.SystemException) { }
                        try { theGrid[r, c + 1].setNextToBomb(); }
                        catch (System.SystemException) { }
                        try { theGrid[r-1, c].setNextToBomb(); }
                        catch (System.SystemException) { }
                        try { theGrid[r+1, c].setNextToBomb(); }
                        catch (System.SystemException) { }
                        try { theGrid[r-1, c - 1].setNextToBomb(); }
                        catch (System.SystemException) { }
                        try { theGrid[r + 1, c - 1].setNextToBomb(); }
                        catch (System.SystemException) { }
                        try { theGrid[r + 1, c + 1].setNextToBomb(); }
                        catch (System.SystemException) { }
                        try { theGrid[r - 1, c + 1].setNextToBomb(); }
                        catch (System.SystemException) { }

                    }
                }
            }
        }
    }
}

﻿using MineSweeperModel;
using System;

namespace Milestone1ConsoleApp
{
    class Program
    {
        static public Board myBoard = new Board(8);
        //public Button[,] btnGrid = new Button[myBoard.Size, myBoard.Size];
        //List<Cell> btnGrid = new List<Cell>();
        //public String piece = "";

        static int gameGoal = 0;
        static int gameProgress = 0;


        static void Main(string[] args)
        {

            myBoard.assignBombs();

            for (int r = 0; r<myBoard.Size; r++)
            {
                for (int c = 0; c<myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb)
                    {
                        Console.Write(" x ");
                    }
                    else if (myBoard.theGrid[r,c].nextToBomb)
                    {
                        Console.Write(" "+myBoard.theGrid[r,c].nextCount+" ");
                    }
                    else
                    {
                        Console.Write(" ~ ");
                    }
                }
                Console.WriteLine();
            }   
        }
    }
}
